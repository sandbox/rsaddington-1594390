Cookie compliance module is in response to the the EU cookie law (http://www.cookielaw.org) and provides both explicit and implicit consent mechanisms. Messaging and location of the mechanism is completely configurable.

This is a port of the Drupal 6 version

Features
--------
* Explicit/implicit consent modes
* Configurable acceptance cookie name
* Configurable user text and links for consent/acceptance/rejection
* Extendable to any modules cookies via hook_cookie_compliance

This module adds a 'splash' message and form for a T&C page.

Installation
------------
1. Copy the module folder to your server.
2. Enable the module via the modules page.

Configuration
-------------

1. General:
    a. the configuration page is at /admin/settings/cookie-compliance (accessible to accounts with the 'administer cookie compliance' permission ONLY).
    b. choose opt explicit/implicit mode.
    c. if required change the default name of the compliance cookie.
    d. set the 'Target div or id' field to the element the splashes to be attached to.
    e. set the urls of the 'more info' & T&C links.

2. Explicit mode only
    a. enable and configure the block titled 'Cookie compliance splash (explicit consent)'. Must be placed in a region that is rendered on all pages.
    b. (optional) change the default text for rejecting consent, rejection reminder and close button.
    c. use the 'Optout reminder' (dis)able the reminder for non-consenting users.


3. Implicit mode only
    a. enable and configure the block titled 'Cookie compliance splash (implicit consent)'. Must be placed in a region that is rendered on all pages.
    b. (optional) change the default text for implied consent.

4. If the T&C block is required:
    a. place the 'Cookie compliance T&C' to display on your T&C page.
    b. alternatively, use the cookie-compliance input filter and use the token [cookie-compliance:tac] on a node body.
