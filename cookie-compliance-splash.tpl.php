<!-- cookie compliance request consent div -->
<div id="cookie-compliance-request-consent" class="cookie-compliance">
    <?php print t($element['cookie-compliance-text-request-consent'], array('!cookie-compliance-url-info' => ($element['cookie-compliance-url-info']))); ?>
    <?php print(drupal_get_form('cookie_compliance_request_consent_form')) ?>
    <p class="cookie-compliance-close"><a href="#" id="cookie-compliance-close"><?php print $element['cookie-compliance-text-ignore']; ?></a></p>
</div>

<!-- cookie compliance optout div -->
<div id="cookie-compliance-optout" class="cookie-compliance">
    <?php print t($element['cookie-compliance-text-optout'], array('!cookie-compliance-url-info' => ($element['cookie-compliance-url-info']))); ?>
    <?php print(drupal_get_form('cookie_compliance_optout_form')) ?>
</div>

<!-- cookie compliance implied div -->
<div id="cookie-compliance-implied" class="cookie-compliance">
    <?php print t($element['cookie-compliance-text-consent-implied'], array('!cookie-compliance-url-tac' => ($element['cookie-compliance-url-tac']))); ?>
</div>
